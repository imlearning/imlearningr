---
title: Archive
permalink: archive/
---

<section id="archive">
  <h3>Most Recent Posts</h3>

  {%for post in site.posts %} 
    {% unless post.next %}

      <ul class="this">
    {% else %} <!-- Capture the year and compare them -->
      {% capture year %}{{ post.date | date: '%Y' }}{% endcapture %}
      {% capture nyear %}{{ post.next.date | date: '%Y' }}{% endcapture %}
      {% if year != nyear %}
        </ul>

        <h3><br/>{{ post.date | date: '%Y' }}</h3> 
        <ul class="past">

      {% endif %}
    {% endunless %}

      <li><a href="{{ site.baseurl }}{{ post.url }}">{{ post.title }}</a>  <time>{{ post.date | date:"%d %b" }}</time></li>

  {% endfor %}
  </ul>
</section>
{% include foot-share.html %}