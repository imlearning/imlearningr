---
layout: post
title: "An Elemental Journey"
description: The opening salvo for this learning journey.
categories: ['R','statistics']
---
# An Elemental Journey
There's nothing special here, folks. I've embarked on a short journey to understand the use of R a bit better. I've got a bit of stats education and practical experience, so I'm not starting out completely blind.

Follow along if you like. Please know that this series will eventually end. Because I'm interested in the practical use of R in everyday analytics (yes, I think it's going to be a reality for all of us), I want it to be a somewhat elementary treatment of statistics, providing the R recipes to accomplish basic analysis.