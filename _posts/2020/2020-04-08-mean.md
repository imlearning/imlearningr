---
layout: post
title: "mean"
description: "Mean use and example."
categories: ['R','statistics']
---

A measure of center. Arithmetic mean, or average. The sum of all values in a list divided by the total number of values present.

In R, _mean_ has a couple of options. You can elect to exclude _NA_ values, which can occur
in lists you get from any source. You can also trim a portion of the values off to resolve
outliers (outliers become the nearest limit value).

## R Example

{% highlight s %}
> lst <- c(1,3,5)
> mean(lst)
[1] 3
{% endhighlight %}