---
layout: post
title:  "sd (Standard Deviation)"
description: "sd use and example."
categories: ['R','statistics']
---

Return the standard deviation of a vector. A measure of shape. It's the expectation of the deviation of a random variable from its mean.

You can opt to remove _NA_ values that exist in the data. According to the R documentation, _sd_ uses n-1 in the denominator.

## R Example

{% highlight s %}
> sd(1:2)
[1] 0.7071068

> sd(1:2) ^ 2
[1] 0.5
{% endhighlight %}
