---
layout: post
title:  "Welcome to I'm Learning R"
description: "Introduction to this content."
date:   2023-01-25 15:32:14 -0300
categories: ["R", "Statistics"]
---

In the spring of 2020 I took several classes in stastics at my local community college. I'd been tasked with Tableau and
Salesforce dashboards at work, and wanted to upgrade my understanding of data theory and pratice. One of the classes made
heavy use of the <span>[R](https://www.r-project.org/)</span> programming language. I found the documentation to be a little terse and
hard to navigate, so I decide to create a simpler set of docs on key R commands that I could use during my study. This blog
is the result of those efforts. Enjoy.
