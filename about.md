---
title: About
permalink: about/
profile: true
---

<img
    class="me"
    alt="{{ author.name }}"
    src="{{ site.author.photo | relative_url }}"
    srcset="{{ site.author.photo2x | relative_url }} 2x"
/>

## In March of 2019...


... I'm tasked at work to create a number of metrics dashboards using the Tableau CRM product
of Salesforce. I decide that as part of this it would be a good idea to bone up on statistics.
So I popped down to my local community college and took three quarters of stats, one of which
made heavy use of R. This blog is a space I used to record my understanding of some of the
more common R commands.

{% include foot-share.html %}